from os import getcwd
from OpenNero import *
from Queue import Queue
from constants import *
from AStar.environment import Environment
from AStar.actor import Actor
from AStar.position import Position

class Block():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.value = 0

class RoombaBrain(AgentBrain):
    """
    Scripted behavior for Roomba agent
    A "lifetime" of an agent brain looks like this:
    1. __init__() is called (new brain)
    2. initialize() is called (passing specs for sensors, actions and rewards)
    3. start() is called
    4. act() is called 0 or more times
    5. end() is called
    6. if new episode, go to 3, otherwise go to 7
    7. destroy() is called
    """
    environment = None
    actor = None

    def __init__(self):
        """
        this is the constructor - it gets called when the brain object is first created
        """
        # call the parent constructor
        AgentBrain.__init__(self) # do not remove!
        self.action_sq = Queue()
        self.minimap = []
          
    def initialize(self, init_info):
        """
        init_info contains a description of the observations the agent will see
        and the actions the agent will be able to perform
        """
        self.init_info = init_info

        # Inicializar el mundo y actor
        resolution = 1
        self.environment = Environment(XDIM, YDIM, resolution, Environment.WALLS, 6 * resolution)

        self.actor = Actor(Position(0, 0), self.environment)

        self.environment.actor_add(self.actor)


        return True

    def start(self, time, sensors):
        """
        Take in the initial sensors and return the first action
        """
        self.actor.restart()

        return self.act(time, sensors, 0)
        
    def act(self, time, sensors, reward):
        """
        Take in new sensors and reward from previous action and return the next action
        Specifically, just move toward the closest crumb!
        """
        action = self.init_info.actions.get_instance()

        # Calcular la accion
        action[0] = self.actor.act(sensors)

        return action
        
    def end(self, time, reward):
        """
        take in final reward
        """
        return True

    def destroy(self):
        """
        called when the agent is destroyed
        """
        self.time = 0
        return True
